-- создание таблицы товар
create table product
(
    id serial primary key,
    description varchar(100),
    cost integer check ( cost > 0 ),
    quantity integer check ( quantity > 0 )
);


-- добавление информации
insert into product(description, cost, quantity)
values ('Поход выходного дня', '3500', 15);

insert into product(description, cost, quantity)
values ('Сплав по реке', '4500', 10);

insert into product(description, cost, quantity)
values ('Тур на авто', '3500', 4);

insert into product(description, cost, quantity)
values ('Скальные занятия', '1500', 11);

insert into product(description, cost, quantity)
values ('Конная прогулка', '5500', 2);

-- создание таблицы заказчик
create table сustomer
(
    id         serial primary key,
    last_name  varchar(20),
    first_name varchar(20)
);

-- добавление информации
insert into сustomer(last_name, first_name)
values ('Горбунов', 'Евгений');
insert into сustomer(last_name, first_name)
values ('Максимов', 'Максим');
insert into сustomer(last_name, first_name)
values ('Хмешинин', 'Михаил');
insert into сustomer(last_name, first_name)
values ('Илизаров', 'Александр');

--создаем таблицу заказов
create table buy
(
    product_id integer,
    customer_id integer,
    date varchar(10),
    count integer check ( count > 0),

    foreign key (product_id) references product (id),
    foreign key (customer_id) references сustomer (id)
);

--заполняем таблицу заказов
insert into buy(product_id, customer_id, "date", "count")
values (1, 2, '06/06/21', 3);

insert into buy(product_id, customer_id, "date", "count")
values (2, 3, '09/05/21', 4);

insert into buy(product_id, customer_id, "date", "count")
values (4, 1, '07/07/21', 3);

insert into buy(product_id, customer_id, "date", "count")
values (3, 4, '08/08/21', 2);

--Написать 3-4 запроса на эти таблицы.
--Вывести все данные
select * from buy inner join сustomer c on c.id = buy.customer_id;
--Вывести все товары и цену
select cost, description from product order by cost desc;
--Вывести всех клиентов и количество их заказов
select last_name, first_name, (select count(*) from buy where customer_id = сustomer.id)
    as buy_count from сustomer;
