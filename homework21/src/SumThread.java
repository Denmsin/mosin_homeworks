import java.util.concurrent.Callable;

public class SumThread extends Thread {
    private int from;
    private int to;
    private int k;

    public SumThread(int from, int to, int k) {
        this.from = from;
        this.to = to;
        this.k = k;
    }
    @Override
    public void run() {
        int sum = 0;
        for (int i = from; i < to; i++) {
            sum += Main.array[i];
        }
        Main.sums[k] = sum;
    }
    }
