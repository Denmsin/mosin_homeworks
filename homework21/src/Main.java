import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int array[];

    public static int sums[];

    public static int sum;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int numbersCount = scanner.nextInt();
        System.out.println("Введите количество потоков");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        System.out.println("Общая сумма - " + realSum);

        // TODO: реализовать работу с потоками

        SumThread[] threads = new SumThread[threadsCount];

        int span = numbersCount / threadsCount;

        for (int i = 0; i < threadsCount; i++) {
            int from = i * span;
            int to = from + span;
            threads[i] = new SumThread(from, to, i);
            threads[i].start();
            try {
                threads[i].join();
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }

        int byThreadSum = 0;
        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }
        System.out.println("Сумма в потоках - " + byThreadSum);
    }
}
