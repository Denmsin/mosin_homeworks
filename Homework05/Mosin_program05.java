import java.util.Scanner;

public class Mosin_program05 {
    public static void main(String[] args) {
	// объявляю объект для считывания чисел с консоли
	Scanner scanner = new Scanner(System.in);
	// считываю первое число
	int a = scanner.nextInt();
	// задать минимуму максимально возможное значение.
	int min = 9;
	// пока не встретили -1
	while (a != -1) {
	    // пока число не равно нулю
	    while (a != 0) {
		// запоминаем последную цифру
		int lastDigit = a % 10;
			    // отбрасываем от исходного числа последнюю цифру
	    a = a / 10; 
	   	    //сравниваем с минимумом
	    if (lastDigit < min) {
		//если новое число меньше придыдущего минимума, то заменяем
		min = lastDigit;
			
	    	}
	    }
	      // на каждом шаге цикла считываем новое число из консоли
	    a = scanner.nextInt();
	}
	// выводим результат
	System.out.println("Result - " + min);

    }

}