import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl {

    List<User> users = new ArrayList<>();

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {

        this.fileName = fileName;
    }

    public List<User> findAll() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader.lines()
                    .map(line -> line.split("\\|"))
                    .map(parts -> new User(Integer.parseInt(parts[0]), parts[1],
                            Integer.parseInt(parts[2]), Boolean.parseBoolean(parts[3])))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public User findById(int id) {
        users = findAll();
        for (User user : users) {
            if (user.getId() == id)
                return user;
        }
        System.out.println("\n" + "ПОЛЬЗОВАТЕЛЬ НЕ НАЙДЕН!" + "\n");
        return null;
    }

    public void update(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (User newUser : users) {
                writer.write(newUser.toString());
                writer.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
