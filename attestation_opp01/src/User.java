public class User {

    private String name;
    private int age;
    private boolean isWorker;
    private int id;

    public User(int id, String name, int age, boolean isWorker) {
        this.name = name;
        this.age = age;
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public void setId(){
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    @Override
    public String toString() {
        return getId() + "|" + getName() + "|" + getAge() + "|" + isWorker();
    }


}
