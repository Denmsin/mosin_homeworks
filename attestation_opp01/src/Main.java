import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        UsersRepositoryFileImpl usersRepository = new UsersRepositoryFileImpl("users.txt");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Начальный список:");
        usersRepository.findAll().forEach(System.out::println);
        User user =  usersRepository.findById(5);

        System.out.println("\n" + "Пользователь найден- " + user.toString() + "\n");

        System.out.println("Введите имя");
        String name = scanner.nextLine();
        user.setName(name);
        System.out.println("Введите возраст");
        int age = scanner.nextInt();
        user.setAge(age);

       usersRepository.update(user);
        System.out.println("\n" + "Обновленный список:");
        usersRepository.findAll().forEach(System.out::println);
    }
}
